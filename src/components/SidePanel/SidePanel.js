import React, { Component } from 'react'
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class SidePanel extends Component {

onListClick=(index)=>{
    this.props.setClassIndex(index)
  }

  render() {
    const classes = this.props.classes;
    const data = this.props.data.data;

    return (
      <Grid xs={3} className={classes.sidePanel}>
        <div className={classes.root}>
            <h1>School:XYZ</h1>
            <List component="nav" aria-label="main mailbox folders">

            {
              data.map((item,index)=>
              <div key={index}>
              <ListItem button onClick={()=>this.onListClick(index)}>
                <ListItemText primary={item.classname}  className={classes.text} />
              </ListItem>


              </div>)
            }

            </List>
        </div>
      </Grid>
    );
  }

}
const styles = theme => ({

  root: {
    maxWidth: 360,
  },
  paper:{
    backgroundColor:'linear-gradient(-45deg, #091befc4 30%, #3a4598bd 90%)'

  },
  sidePanel:{
    minHeight:'100vh',
    background: 'linear-gradient(-45deg, #091befc4 30%, #3a4598bd 90%)',

  },
  text:{
    color:'white'
  }


})

export default withStyles(styles)(SidePanel);
