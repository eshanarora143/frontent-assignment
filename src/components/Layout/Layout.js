import Grid from '@material-ui/core/Grid';
import MainPanel from '../MainPanel/mainPanelContainer';
import SidePanel from '../SidePanel/sidePanelContainer';
import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core';


class Layout extends Component {


  componentDidMount(){
  }


  render() {
    const classes = this.props.classes;

    return (
      <div >
      <Grid container>
        <SidePanel/>
        <MainPanel />
      </Grid>
      </div>
    );
  }

}
const styles = theme => ({


})

export default withStyles(styles)(Layout);
