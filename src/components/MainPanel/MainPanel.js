import React, { Component } from 'react'
import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import classnames from 'classnames';
import Container from '@material-ui/core/Container';
import LinearProgress from '@material-ui/core/LinearProgress';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: ('red', 0.3),
    width:100,
    borderRadius:20
  },
  bar: {
    borderRadius: 20,
    backgroundColor: 'red',
  },
})(LinearProgress);


class MainPanel extends Component {

  averageMark= (a,b,c)=>{
    return parseInt((a+b+c)/3)
  }

  render() {
    const classes = this.props.classes;
    const index = this.props.data.selectedClass;

    console.log(this.props.data.data[index]);
    const data =  this.props.data.data[index];

    // const  students = this.props.data.data[index].students;
    const fixedHeightPaper = classnames(classes.paper, classes.fixedHeight);
    function average(){
      var i;
      var averageClass= 0;
      for (i = 0; i < data.students.length; i++) {
        averageClass +=parseInt(((data.students[i].marks.English+data.students[i].marks.Science+data.students[i].marks.Maths)/3)/data.students.length);
      }
      return averageClass;
  }




    return (
      <Grid xs={9} className={classes.mainPanel}>
        <div >
          <Paper className={classes.root}>
            <main className={classes.content}>
              <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3} justify="space-between">
                  {
                    data?
                    <>
                    <ExpansionPanel className={classes.expand}>
                      <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                      >
                      <Grid container>
                        <Grid xs={8}>
                          <Typography className={classes.heading}>{data.classname}<br/>{data.students.length} students</Typography>
                        </Grid>
                        <Grid xs={4}>
                          Show average

                        </Grid>


                      </Grid>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                        <Typography className={classes.typography}>
                          the average performance of the class is {
                            average()
                          }%
                        </Typography>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <Divider/>
                    </>
                    :
                    <h1 className={classes.selectClass}>Select a Class</h1>


                  }
                  {
                    data?

                    data.students.map((student,index)=>
                    <div>


                    <Grid item xs={12} md={3} lg={5} key={index} className={classes.grid}>
                      <Paper className={fixedHeightPaper} >
                        <Grid container spacing={3} justify="space-between" >
                          <Grid xs={11}  >
                          {student.name}
                          </Grid>

                          <Grid xs={1} >
                          <Typography className={classes.average}>
                          {
                            this.averageMark(student.marks.Maths,student.marks.Science,student.marks.English)
                          }%
                          </Typography>
                          </Grid>
                          <Grid xs={3} className={classes.subjectName}>
                          Maths
                          </Grid>
                          <Grid xs={7} className={classes.bar} >
                          <BorderLinearProgress
                            className={classes.margin}
                            variant="determinate"
                            color="secondary"
                            value={student.marks.Maths}
                          />
                          </Grid>
                          <Grid xs={2} className={classes.marks}>
                          {student.marks.Maths}%
                          </Grid>
                          <Grid xs={3} className={classes.subjectName}>
                          Science
                          </Grid>
                          <Grid xs={7} className={classes.bar}>
                          <BorderLinearProgress
                            className={classes.margin}
                            variant="determinate"
                            color="secondary"
                            value={student.marks.Science}
                          />

                          </Grid>
                          <Grid xs={2} className={classes.marks}>
                          {student.marks.Science}%
                          </Grid>
                          <Grid xs={3}  className={classes.subjectName}>
                          English
                          </Grid>
                          <Grid xs={7} className={classes.bar}>
                          <BorderLinearProgress
                            className={classes.margin}
                            variant="determinate"
                            color="secondary"
                            value={student.marks.English}
                          />

                          </Grid>
                          <Grid xs={2} className={classes.marks}>
                          {student.marks.English}%
                          </Grid>




                        </Grid>
                      </Paper>
                    </Grid>

                    </div>)
                    :
                    ""

                  }



                </Grid>
              </Container>
            </main>
          </Paper>
        </div>
      </Grid>
    );
  }

}
const styles = theme => ({

  root: {
    minHeight:'100vh',
    padding:50,
    backgroundColor:'#eeeeee'

  },
  mainPanel:{
    minHeight:'100vh'

  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    borderRadius:10
  },
  fixedHeight: {
    height: 190,
    width:275,
    margin:20,
    padding:30
},
  expand:{
    width:'100%',
    boxShadow:'none',
    borderRadius:20
  },

  grid:{
    padding:10
  },
  subjectName:{
    paddingTop:20,
  },
  marks:{
    paddingTop:20,

  },
  bar:{
    paddingTop:25
  },
  list:{
    paddingTop:20
  },
  selectClass:{
    padding:250
  },
  typography:{
    paddingLeft:200,
    fontWeight: 'bold',

  },
  average:{
    color:'#29b6f6',
    fontWeight: 'bold',
    fontSize: 20,

  }


})

export default withStyles(styles)(MainPanel);
