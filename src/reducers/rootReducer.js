import data from '../classroom_data.json';
const initState = {
  data,
  selectedClass:null,
  average:null,
};
export const SELECT_CLASS = "SELECT_CLASS";

export const selectClass = (index) =>  ({
  type: SELECT_CLASS,
  index
});


export const setClassIndex = (index) => dispatch => {

    dispatch(selectClass(index));


};



const rootReducer = (state = initState, {type,index}) => {
 if(type === 'SELECT_CLASS'){

   return {
     ...state,
     selectedClass: index
   }
  }

  return state;
}

export default rootReducer
